package com.auditoria.proyecto.auditoria.business;

import java.util.ArrayList; 
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.auditoria.proyecto.auditoria.model.Apartado;
import com.auditoria.proyecto.auditoria.model.AuditoriaEmpresa;
import com.auditoria.proyecto.auditoria.model.ItemCuestionario;
import com.auditoria.proyecto.auditoria.model.RespuestaAuditoria;
import com.auditoria.proyecto.auditoria.repository.AuditoriaEmpresaRepository;
import com.auditoria.proyecto.auditoria.repository.RespuestaAuditoriaRepository;
import com.auditoria.proyecto.auditoria.util.Constante;

@Service
public class AuditoriaEmpresaManager {

	@Autowired
	@Qualifier("auditoriaEmpresaRepository")
	private AuditoriaEmpresaRepository auditoriaEmpresaRepository;

	@Autowired
	private RespuestaAuditoriaRepository respuestaAuditoriaRepository;

	public void save(AuditoriaEmpresa auditoriaEmpresa) {
		
		
		RespuestaAuditoria respuesta = null;
		if (auditoriaEmpresa.getIdAuditoriaEmpresa() == null || auditoriaEmpresa.getIdAuditoriaEmpresa() == 0) {
			auditoriaEmpresa.setStatus(Constante.NO_APROBADO);
			auditoriaEmpresaRepository.save(auditoriaEmpresa);
			/*
			 * for (ItemCuestionario item : auditoriaEmpresa.getNorma().getItems()) {
			 * respuesta = new RespuestaAuditoria();
			 * respuesta.setAuditoriaEmpresa(auditoriaEmpresa);
			 * respuesta.setRespuestaMarcada(""); respuesta.setResultado("");
			 * respuesta.setItemCuestionario(item);
			 * respuestaAuditoriaRepository.save(respuesta); }
			 */
			for (Apartado apartado : auditoriaEmpresa.getNorma().getApartados()) {
				for (ItemCuestionario item : apartado.getItems()) {
					respuesta = new RespuestaAuditoria();
					respuesta.setAuditoriaEmpresa(auditoriaEmpresa);
					respuesta.setRespuestaMarcada("");
					respuesta.setResultado("");
					respuesta.setItemCuestionario(item);
					respuestaAuditoriaRepository.save(respuesta);
				}
			}

		} else {
			
			auditoriaEmpresaRepository.save(auditoriaEmpresa);
		}

	}

	public AuditoriaEmpresa findById(Long id) {
		return auditoriaEmpresaRepository.findById(id).orElse(new AuditoriaEmpresa());
	}

	public List<AuditoriaEmpresa> findAll() {
		Iterable<AuditoriaEmpresa> auditoriasEmpresaIterable = auditoriaEmpresaRepository.findAll();
		List<AuditoriaEmpresa> auditorias = new ArrayList<AuditoriaEmpresa>();
		auditoriasEmpresaIterable.forEach(auditorias::add);
		return auditorias;
	}

	public void deleteById(Long id) {
		auditoriaEmpresaRepository.deleteById(id);
	}

}
