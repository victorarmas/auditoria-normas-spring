package com.auditoria.proyecto.auditoria.repository;

import org.springframework.data.repository.CrudRepository;

import com.auditoria.proyecto.auditoria.model.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long>{

	Usuario findByUsernameAndPassword(String username, String password);
	
	
}
