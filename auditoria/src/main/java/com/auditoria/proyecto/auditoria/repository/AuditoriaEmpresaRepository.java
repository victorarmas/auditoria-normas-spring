package com.auditoria.proyecto.auditoria.repository;

import org.springframework.data.repository.CrudRepository;

import com.auditoria.proyecto.auditoria.model.AuditoriaEmpresa;

public interface AuditoriaEmpresaRepository extends CrudRepository<AuditoriaEmpresa, Long>{

}
