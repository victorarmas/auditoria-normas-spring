package com.auditoria.proyecto.auditoria.web;

import java.sql.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.auditoria.proyecto.auditoria.business.AuditoriaEmpresaManager;
import com.auditoria.proyecto.auditoria.business.EmpresaManager;
import com.auditoria.proyecto.auditoria.business.NormaManager;
import com.auditoria.proyecto.auditoria.model.AuditoriaEmpresa;
import com.auditoria.proyecto.auditoria.model.Empresa;
import com.auditoria.proyecto.auditoria.model.Norma;
import com.auditoria.proyecto.auditoria.model.Usuario;
import com.auditoria.proyecto.auditoria.util.Util;
import com.auditoria.proyecto.auditoria.viewModel.AuditoriaEmpresaViewModel;

@Controller
public class AuditoriaEmpresaController {

	@Autowired
	private AuditoriaEmpresaManager auditoriaEmpresaManager;
	
	@Autowired
	private NormaManager normaManager;
	
	@Autowired
	private EmpresaManager empresaManager;
	
	@Autowired
	private Util util;
	
	
	/**
	 * 
	 * @param model
	 * @return a vista de la lista de auditorias
	 */
	@GetMapping("/auditorias")
	public String getAuditorias(Model model) {
		model.addAttribute("auditorias", auditoriaEmpresaManager.findAll());
		return "auditorias";
	}
	
	/**
	 * 
	 * @param model
	 * @return a formulario de creacion de una nueva auditoria
	 */
	@GetMapping("/auditoria/new")
	public String newAuditoria(Model model) {
		model.addAttribute("normas" , normaManager.findAll());
		model.addAttribute("empresas" , empresaManager.findAll());
		model.addAttribute("auditoria", new AuditoriaEmpresaViewModel());
		
		return "auditoriaForm";
	}
	
	/**
	 * Crea una nueva auditoria a una empresa
	 * @param auditoriaViewModel
	 * @param model
	 * @return a vista de la lista de auditorias
	 */
	@PostMapping("/auditoria")
	public String crearAuditoria(AuditoriaEmpresaViewModel auditoriaViewModel, Model model, HttpServletRequest request) {
		Norma norma = normaManager.findById(auditoriaViewModel.getIdNorma());
		Empresa empresa = empresaManager.findById(auditoriaViewModel.getIdEmpresa());
		Usuario usuarioLogueado = util.getUsuarioLogueado(request);
		
		System.out.println("==============USUARIO LOGUEADO================");
		System.out.println(usuarioLogueado);
		System.out.println("==============================");
		
		AuditoriaEmpresa auditoriaEmpresa = new AuditoriaEmpresa();
		auditoriaEmpresa.setIdAuditoriaEmpresa(auditoriaViewModel.getIdAuditoria());
		auditoriaEmpresa.setEmpresa(empresa);
		auditoriaEmpresa.setNorma(norma);
		auditoriaEmpresa.setUsuarioAplicador(usuarioLogueado);
		auditoriaEmpresa.setFecha(new java.sql.Date(new java.util.Date().getTime()));
		
		auditoriaEmpresaManager.save(auditoriaEmpresa);
		
		return "redirect:/auditorias";
	}
	
	/**
	 * 
	 * @param id
	 * @param model
	 * @return vista del detalle de la auditoria
	 */
	@GetMapping("/auditoria/{id}")
	public String getAuditoriaById(@PathVariable Long id, Model model) {
		model.addAttribute("auditoria", auditoriaEmpresaManager.findById(id));
		return "auditoria";
	}
	
	/**
	 * 
	 * @param id
	 * @param model
	 * @return a vista de formulario de edicion de auditoria
	 */
	@GetMapping("/auditoria/edit/{id}")
	public String editAuditoria(@PathVariable Long id, Model model) {
		AuditoriaEmpresa auditoria = auditoriaEmpresaManager.findById(id);
		AuditoriaEmpresaViewModel auditoriaViewModel = 
				new AuditoriaEmpresaViewModel(auditoria.getIdAuditoriaEmpresa(), auditoria.getNorma().getIdNorma()
						,auditoria.getEmpresa().getIdEmpresa(), auditoria.getFecha());
		model.addAttribute("normas" , normaManager.findAll());
		model.addAttribute("empresas" , empresaManager.findAll());
		model.addAttribute("auditoria", auditoriaViewModel);
		
		return "auditoriaForm";
	}
	
	/**
	 * Actualiza una auditoria
	 * @param auditoriaViewModel
	 * @param model
	 * @return a vista de la lista de auditorias
	 */
	@PostMapping("/auditoria/{id}")
	public String updateAuditoria(AuditoriaEmpresaViewModel auditoriaViewModel, Model model, HttpServletRequest request) {
		Norma norma = normaManager.findById(auditoriaViewModel.getIdNorma());
		Empresa empresa = empresaManager.findById(auditoriaViewModel.getIdEmpresa());
		Usuario usuarioLogueado = util.getUsuarioLogueado(request);
		
		AuditoriaEmpresa auditoriaEmpresa = new AuditoriaEmpresa();
		auditoriaEmpresa.setIdAuditoriaEmpresa(auditoriaViewModel.getIdAuditoria());
		auditoriaEmpresa.setEmpresa(empresa);
		auditoriaEmpresa.setNorma(norma);
		auditoriaEmpresa.setUsuarioAplicador(usuarioLogueado);
		auditoriaEmpresa.setFecha(new java.sql.Date(new java.util.Date().getTime()));
		
		auditoriaEmpresaManager.save(auditoriaEmpresa);
		
		return "redirect:/auditorias";
	}
	
	/**
	 * Elimina una auditoria
	 * @param id
	 * @return a la vista de auditorias
	 */
	@GetMapping("/auditoria/delete/{id}")
	public String deteleAuditoriaById(@PathVariable Long id) {
		auditoriaEmpresaManager.deleteById(id);
		return "redirect:/auditorias";
	}
	
}
