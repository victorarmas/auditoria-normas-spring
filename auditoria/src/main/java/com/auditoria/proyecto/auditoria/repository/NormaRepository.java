package com.auditoria.proyecto.auditoria.repository;
import com.auditoria.proyecto.auditoria.model.Norma;
import org.springframework.data.repository.CrudRepository;

public interface NormaRepository extends CrudRepository<Norma,Long>{

}
