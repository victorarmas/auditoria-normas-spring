package com.auditoria.proyecto.auditoria.repository;

import org.springframework.data.repository.CrudRepository;

import com.auditoria.proyecto.auditoria.model.ItemCuestionario;

public interface ItemCuestionarioRepository extends CrudRepository<ItemCuestionario, Long>{

}
