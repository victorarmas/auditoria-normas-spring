package com.auditoria.proyecto.auditoria.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

	//@Autowired
	
	@GetMapping("/home")
	public String home(Model model) {
		return "home";
	}
}
