package com.auditoria.proyecto.auditoria.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "items_cuestionario")
public class ItemCuestionario {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idItemCuestionario;
	
	//@ManyToOne(cascade=CascadeType.ALL)
	@ManyToOne
	@JoinColumn(name="id_apartado")
	private Apartado apartado;
	
	private String item;
	private String respuestaDeseada;
	
	public ItemCuestionario() {
	}
	
	

	public ItemCuestionario(Long idItemCuestionario, Apartado apartado, String item, String respuestaDeseada) {
		super();
		this.idItemCuestionario = idItemCuestionario;
		this.apartado = apartado;
		this.item = item;
		this.respuestaDeseada = respuestaDeseada;
	}
	
	

	public Long getIdItemCuestionario() {
		return idItemCuestionario;
	}



	public void setIdItemCuestionario(Long idItemCuestionario) {
		this.idItemCuestionario = idItemCuestionario;
	}



	public Apartado getApartado() {
		return apartado;
	}



	public void setApartado(Apartado apartado) {
		this.apartado = apartado;
	}



	public String getItem() {
		return item;
	}



	public void setItem(String item) {
		this.item = item;
	}



	public String getRespuestaDeseada() {
		return respuestaDeseada;
	}



	public void setRespuestaDeseada(String respuestaDeseada) {
		this.respuestaDeseada = respuestaDeseada;
	}



	@Override
	public String toString() {
		return "ItemCuestionario [idItemCuestionario=" + idItemCuestionario  + ", item=" + item
				+ ", respuestaDeseada=" + respuestaDeseada + "]";
	}
	
	
	
}
