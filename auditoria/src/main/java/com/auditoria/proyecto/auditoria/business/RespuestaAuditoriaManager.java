package com.auditoria.proyecto.auditoria.business;

import java.util.Iterator; 
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.auditoria.proyecto.auditoria.model.AuditoriaEmpresa;
import com.auditoria.proyecto.auditoria.model.RespuestaAuditoria;
import com.auditoria.proyecto.auditoria.repository.AuditoriaEmpresaRepository;
import com.auditoria.proyecto.auditoria.repository.RespuestaAuditoriaRepository;
import com.auditoria.proyecto.auditoria.util.Constante;
import com.auditoria.proyecto.auditoria.util.Util;

@Service
public class RespuestaAuditoriaManager {

	@Autowired
	private RespuestaAuditoriaRepository respuestaAuditoriaRepository;
	
	@Autowired
	@Qualifier("auditoriaEmpresaRepository")
	private AuditoriaEmpresaRepository auditoriaRepository;
	
	@Autowired
	private Util util;
	
	public void save(RespuestaAuditoria respuestaAuditoria) {
		respuestaAuditoriaRepository.save(respuestaAuditoria);
	}
	
	public void updateAllRespuestas(AuditoriaEmpresa auditoria, List<Long> listaId) {
		int size = auditoria.getRespuestas().size();
		//for(RespuestaAuditoria respuesta : auditoria.getRespuestas()) {
		//for(Iterator<RespuestaAuditoria> iterator = auditoria.getRespuestas().iterator(); iterator.hasNext();) {
		for(int i = 0; i < size; i++) {
			//RespuestaAuditoria respuesta = iterator.next();
			RespuestaAuditoria respuesta = auditoria.getRespuestas().get(i);
			if(util.foundInList(listaId, respuesta.getIdRespuestaAuditoria())) {
				respuesta.setRespuestaMarcada(Constante.SI);
			}else {
				respuesta.setRespuestaMarcada(Constante.NO);
			}
			if(respuesta.getItemCuestionario().getRespuestaDeseada().equals(respuesta.getRespuestaMarcada())) {
				respuesta.setResultado(Constante.CUMPLE);
			}else {
				respuesta.setResultado(Constante.NO_CUMPLE);
			}
			respuestaAuditoriaRepository.save(respuesta);
		}
		
		
		
		String status = auditoria.getRespuestas().stream()
				.filter(x -> Constante.NO_CUMPLE.equalsIgnoreCase(x.getResultado()))
				.collect(Collectors.toList()).size() > 0 ? Constante.NO_APROBADO : Constante.APROBADO;
		auditoria.setStatus(status);
		
		System.out.println("========================");
		System.out.println("========================STATUS[" + status +"]");
		System.out.println("========================");
		
		auditoriaRepository.save(auditoria);
		
		//respuestaAuditoriaRepository.saveAll(auditoria.getRespuestas());
		
		
	}
	
}
