package com.auditoria.proyecto.auditoria.web;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.auditoria.proyecto.auditoria.business.AuditoriaEmpresaManager;
import com.auditoria.proyecto.auditoria.business.RespuestaAuditoriaManager;
import com.auditoria.proyecto.auditoria.model.AuditoriaEmpresa;

@Controller
public class RespuestaAuditoriaController {

	@Autowired
	private RespuestaAuditoriaManager respuestaAuditoriaManager;
	
	@Autowired
	private AuditoriaEmpresaManager auditoriaEmpresaManager;
	
	/**
	 * 
	 * @param id
	 * @param model
	 * @return formulario de evaluacion de auditoria (CheckList)
	 */
	@GetMapping("/evaluacion/{id}")
	public String getEvaluacion(@PathVariable Long id, Model model) {
		AuditoriaEmpresa auditoriaEmpresa = auditoriaEmpresaManager.findById(id);
		model.addAttribute("auditoriaEmpresa" , auditoriaEmpresa);
		return "evaluacion";
	}
	
	@PostMapping("/evaluacion")
	public String updateEvaluacion(@RequestParam("idChecked") List<String> idrespuestas
			,AuditoriaEmpresa auditoriaEmpresa) {
		auditoriaEmpresa = auditoriaEmpresaManager.findById(auditoriaEmpresa.getIdAuditoriaEmpresa());
		List<Long> listaId = new ArrayList<Long>();
		for(String idrespuesta : idrespuestas){
            Long idres = Long.parseLong(idrespuesta);
            listaId.add(idres);
        } 
		respuestaAuditoriaManager.updateAllRespuestas(auditoriaEmpresa, listaId);
		return "redirect:/evaluacion/" + auditoriaEmpresa.getIdAuditoriaEmpresa();
	}
}
