package com.auditoria.proyecto.auditoria.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "apartados")
public class Apartado {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idApartado;
	
	//@ManyToOne(cascade=CascadeType.ALL)
	@ManyToOne
	@JoinColumn(name="id_norma")
	private Norma norma;
	
	@OneToMany(targetEntity=ItemCuestionario.class, mappedBy="apartado", fetch=FetchType.LAZY)
	private List<ItemCuestionario> items = new ArrayList<ItemCuestionario>() ;
	
	private String nombre;
	
	public Apartado() {
	}

	public Apartado(Long idApartado, Norma norma, List<ItemCuestionario> items, String nombre) {
		super();
		this.idApartado = idApartado;
		this.norma = norma;
		this.items = items;
		this.nombre = nombre;
	}

	public Long getIdApartado() {
		return idApartado;
	}

	public void setIdApartado(Long idApartado) {
		this.idApartado = idApartado;
	}

	public Norma getNorma() {
		return norma;
	}

	public void setNorma(Norma norma) {
		this.norma = norma;
	}

	public List<ItemCuestionario> getItems() {
		return items;
	}

	public void setItems(List<ItemCuestionario> items) {
		this.items = items;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}
