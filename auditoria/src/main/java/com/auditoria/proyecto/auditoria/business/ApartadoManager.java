package com.auditoria.proyecto.auditoria.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.auditoria.proyecto.auditoria.model.Apartado;
import com.auditoria.proyecto.auditoria.repository.ApartadoRepository;

@Service
public class ApartadoManager {

	@Autowired
	private ApartadoRepository apartadoRepository;
	
	public List<Apartado> findAll(){
		Iterable<Apartado> apartadosIterable = apartadoRepository.findAll();
		List<Apartado> apartados = new ArrayList<Apartado>();
		apartadosIterable.forEach(apartados::add);
		return apartados;
	}
	
	public Apartado findById(Long id) {
		return apartadoRepository.findById(id).orElse(new Apartado());
	}
	
	public void save(Apartado apartado) {
		apartadoRepository.save(apartado);
	}
	
	public void deleteById(Long id) {
		apartadoRepository.deleteById(id);
	}
}
