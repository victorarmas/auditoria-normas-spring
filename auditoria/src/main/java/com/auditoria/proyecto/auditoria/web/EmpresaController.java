package com.auditoria.proyecto.auditoria.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.auditoria.proyecto.auditoria.business.EmpresaManager;
import com.auditoria.proyecto.auditoria.model.Empresa;

@Controller
public class EmpresaController {

	@Autowired
	EmpresaManager empresaManager;
	
	/**
	 * 
	 * @param model
	 * @return a vista del listado de empresas
	 */
	@GetMapping("/empresas")
	public String getEmpresas(Model model) {
		model.addAttribute("empresas", empresaManager.findAll());
		return "empresas";
	}
	
	/**
	 * 
	 * @param model
	 * @return a formulario para crear una nueva empresa
	 */
	@GetMapping("/empresa/new")
	public String newEmpresa(Model model) {
		model.addAttribute("empresa", new Empresa());
		return "empresaForm";
	}
	
	/**
	 * Registra una nueva empresa
	 * @param empresa
	 * @param model
	 * @return a vista de listado de las empresas
	 */
	@PostMapping("/empresa")
	public String crearEmpresa(Empresa empresa, Model model) {
		empresaManager.saveEmpresa(empresa);
		return "redirect:/empresas";
	}
	
	/**
	 * 
	 * @param id
	 * @param model
	 * @return a vista para ver detalle de la empresa (No editar)
	 */
	@GetMapping("/empresa/{id}")
	public String getEmpresaById(@PathVariable Long id, Model model) {
		model.addAttribute("empresa", empresaManager.findById(id));
		return "empresa";
	}
	
	/**
	 * 
	 * @param id
	 * @param model
	 * @return a formulario de actualizacion de Empresa
	 */
	@GetMapping("/empresa/edit/{id}")
	public String editEmpresa(@PathVariable Long id, Model model) {
		model.addAttribute("empresa", empresaManager.findById(id));
		return "empresaForm";
	}
	
	/**
	 * Actualiza una empresa
	 * @param empresa
	 * @param model
	 * @return a vista de todas las empresas
	 */
	@PostMapping("/empresa/{id}")
	public String updateEmpresa(Empresa empresa, Model model) {
		empresaManager.saveEmpresa(empresa);
		return "redirect:/empresas";
	}
	
	/**
	 * Elimina una empresa segun el id indicado
	 * @param id
	 * @return a vista de todas las empresas
	 */
	@GetMapping("/empresa/delete/{id}")
	public String deleteEmpresa(@PathVariable Long id) {
		empresaManager.deleteById(id);
		return "redirect:/empresas";
	}
}
