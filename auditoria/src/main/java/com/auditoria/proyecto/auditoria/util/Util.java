package com.auditoria.proyecto.auditoria.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.auditoria.proyecto.auditoria.model.AuditoriaEmpresa;
import com.auditoria.proyecto.auditoria.model.RespuestaAuditoria;
import com.auditoria.proyecto.auditoria.model.Usuario;

@Component
public class Util {
	
	public static int index = -1;
	
	public String getMD5EncryptedValue(String password) {
        final byte[] defaultBytes = password.getBytes();
        try {
            final MessageDigest md5MsgDigest = MessageDigest.getInstance("MD5");
            md5MsgDigest.reset();
            md5MsgDigest.update(defaultBytes);
            final byte messageDigest[] = md5MsgDigest.digest();
            final StringBuffer hexString = new StringBuffer();
            for (final byte element : messageDigest) {
                final String hex = Integer.toHexString(0xFF & element);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }
            password = hexString + "";
        } catch (final NoSuchAlgorithmException nsae) {
            nsae.printStackTrace();
        }
        return password;
    }
	
	public boolean foundInList(List<Long> lista, Long item) {
		return lista.contains(item);
	}
	
	public static int getNextIndex() {
		index++;
		return index;
	}
	
	public static int getIndex() {
		return index;
	}
	
	public static int restartIndex() {
		index = -1;
		return index;
	}
	
	public Usuario getUsuarioLogueado(HttpServletRequest request) {
		Object uObject = request.getSession().getAttribute("usuarioLogueado");
		
		if(!(uObject instanceof Usuario) ) {
			return null;
		}
		
		return (Usuario) uObject;
	}
	
	public double getPorcentajeAciertos(AuditoriaEmpresa auditoria) {
		
		List<RespuestaAuditoria> respuestas = auditoria.getRespuestas();
		
		int totalPreguntas = respuestas.size();
		
		double aciertos = respuestas.stream()
							.filter(x -> Constante.CUMPLE.equals(x.getResultado()))
							.collect(Collectors.toList()).size();
		
		return (aciertos/totalPreguntas) * 100;
	}
	
	
}
