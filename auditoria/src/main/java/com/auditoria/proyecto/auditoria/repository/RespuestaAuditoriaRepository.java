package com.auditoria.proyecto.auditoria.repository;

import org.springframework.data.repository.CrudRepository;

import com.auditoria.proyecto.auditoria.model.RespuestaAuditoria;

public interface RespuestaAuditoriaRepository extends CrudRepository<RespuestaAuditoria,Long>{

}
