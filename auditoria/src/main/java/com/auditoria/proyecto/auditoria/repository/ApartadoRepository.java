package com.auditoria.proyecto.auditoria.repository;

import org.springframework.data.repository.CrudRepository;

import com.auditoria.proyecto.auditoria.model.Apartado;

public interface ApartadoRepository extends CrudRepository<Apartado,Long>{

}
