package com.auditoria.proyecto.auditoria.repository;

import org.springframework.data.repository.CrudRepository;

import com.auditoria.proyecto.auditoria.model.Empresa;

public interface EmpresaRepository extends CrudRepository<Empresa,Long>{

}
