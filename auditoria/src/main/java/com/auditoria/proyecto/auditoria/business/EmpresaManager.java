package com.auditoria.proyecto.auditoria.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.auditoria.proyecto.auditoria.model.Empresa;
import com.auditoria.proyecto.auditoria.repository.EmpresaRepository;

@Service
public class EmpresaManager {
	
	@Autowired
	private EmpresaRepository empresaRepository;
	
	public void saveEmpresa(Empresa empresa) {
		empresaRepository.save(empresa);
	}
	
	public Empresa findById(Long id) {
		return empresaRepository.findById(id).orElse(new Empresa());
	}
	
	public List<Empresa> findAll(){
		Iterable<Empresa> empresasIterable = empresaRepository.findAll();
		List<Empresa> empresas = new ArrayList<Empresa>();
		empresasIterable.forEach(empresas::add);
		System.out.println("===================");
		System.out.println(empresas);
		System.out.println("===================");
		return empresas;
	}
	
	public void deleteById(Long id) {
		empresaRepository.deleteById(id);
	}
	
}
