package com.auditoria.proyecto.auditoria.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.auditoria.proyecto.auditoria.business.UsuarioManager;
import com.auditoria.proyecto.auditoria.model.Empresa;
import com.auditoria.proyecto.auditoria.model.Usuario;

@Controller
public class UsuarioController {

	@Autowired
	private UsuarioManager usuarioManager;
	
	/**
	 * 
	 * @param model
	 * @return a vista de lista de usuarios
	 */
	@GetMapping("/usuarios")
	public String getUsuarios(Model model) {
		model.addAttribute("usuarios", usuarioManager.findAll());
		return "usuarios";
	}
	
	/**
	 * 
	 * @param model
	 * @return a formulario para registrar un nuevo usuario
	 */
	@GetMapping("/usuario/new")
	public String newEmpresa(Model model) {
		model.addAttribute("usuario", new Usuario());
		return "usuarioForm";
	}
	
	/**
	 * Registra un nuevo usuario
	 * @param usuario
	 * @param model
	 * @return a vista de lista de usuarios
	 */
	@PostMapping("/usuario")
	public String createUsuario(Usuario usuario, Model model) {
		usuarioManager.save(usuario);
		return "redirect:/usuarios";
	}
	
	/**
	 * 
	 * @param id
	 * @param model
	 * @return a vista para ver el detalle del usuario (No vista editar)
	 */
	@GetMapping("/usuario/{id}")
	public String getUsuarioById(@PathVariable Long id, Model model) {
		model.addAttribute("usuario", usuarioManager.findById(id));
		return "usuario";
	}
	
	/**
	 * 
	 * @param id
	 * @param model
	 * @return a formulario de edicion de un usuario
	 */
	@GetMapping("/usuario/edit/{id}")
	public String editUsuario(@PathVariable Long id, Model model) {
		model.addAttribute("usuario", usuarioManager.findById(id));
		return "usuarioForm";
	}
	
	/**
	 * Actualiza la informacion de un usuario
	 * @param usuario
	 * @param model
	 * @return a vista de lista de usuarios
	 */
	@PostMapping("/usuario/{id}")
	public String updateUsuario(Usuario usuario, Model model) {
		usuarioManager.save(usuario);
		return "redirect:/usuarios";
	}
	
	/**
	 * Elimina un usuario
	 * @param id
	 * @return a vista de lista de usuarios
	 */
	@GetMapping("/usuario/delete/{id}")
	public String deteleUsuario(@PathVariable Long id) {
		usuarioManager.deleteById(id);
		return "redirect:/usuarios";
	}
	
}
