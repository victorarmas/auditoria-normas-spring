package com.auditoria.proyecto.auditoria.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


public class AutorizadorInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, 
            HttpServletResponse response,
            Object controller) throws Exception {

            String uri = request.getRequestURI();
            if(uri.endsWith("/")) {
            	response.sendRedirect("home");
                return false;
            }
            if(uri.endsWith("login") ||
                    uri.endsWith("login") || 
                             uri.contains("resources")){
                return true;
            }

            if(request.getSession()
                    .getAttribute("usuarioLogueado") != null) {
                return true;
            }

            response.sendRedirect("login");
            return false;
    }
}
