package com.auditoria.proyecto.auditoria.viewModel;

public class ApartadoViewModel {

	private Long idNorma;
	private String nombre;
	
	public ApartadoViewModel() {
	}

	public ApartadoViewModel(Long idNorma, String nombre) {
		super();
		this.idNorma = idNorma;
		this.nombre = nombre;
	}

	public Long getIdNorma() {
		return idNorma;
	}

	public void setIdNorma(Long idNorma) {
		this.idNorma = idNorma;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}
