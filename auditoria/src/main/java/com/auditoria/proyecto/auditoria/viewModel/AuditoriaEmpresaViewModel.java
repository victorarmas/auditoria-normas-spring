package com.auditoria.proyecto.auditoria.viewModel;

import java.sql.Date;

public class AuditoriaEmpresaViewModel {

	private Long idAuditoria;
	private Long idNorma;
	private Long idEmpresa;
	private Date fecha;
	
	public AuditoriaEmpresaViewModel() {
	}

	public AuditoriaEmpresaViewModel(Long idAuditoria, Long idNorma, Long idEmpresa, Date fecha) {
		super();
		this.idAuditoria = idAuditoria;
		this.idNorma = idNorma;
		this.idEmpresa = idEmpresa;
		this.fecha = fecha;
	}

	public Long getIdAuditoria() {
		return idAuditoria;
	}

	public void setIdAuditoria(Long idAuditoria) {
		this.idAuditoria = idAuditoria;
	}

	public Long getIdNorma() {
		return idNorma;
	}

	public void setIdNorma(Long idNorma) {
		this.idNorma = idNorma;
	}

	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	
	
}
