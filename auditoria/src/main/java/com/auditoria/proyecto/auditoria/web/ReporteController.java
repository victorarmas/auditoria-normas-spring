package com.auditoria.proyecto.auditoria.web;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.auditoria.proyecto.auditoria.business.AuditoriaEmpresaManager;
import com.auditoria.proyecto.auditoria.model.AuditoriaEmpresa;
import com.auditoria.proyecto.auditoria.reporte.ReporteAuditoria;

@RestController
public class ReporteController {

	@Autowired
	private AuditoriaEmpresaManager auditoriaEmpresaManager;
	
	@Autowired
	private ReporteAuditoria reporteAuditoria;
	
	@RequestMapping(value = "/reporte", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> auditoriasReporte() throws IOException {
		
		List<AuditoriaEmpresa> auditorias = auditoriaEmpresaManager.findAll();
		
		ByteArrayInputStream bis = reporteAuditoria.reporteAuditorias(auditorias);
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=auditorias.pdf");
        
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
		
	}
	
	@RequestMapping(value = "/report", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> auditoriasReporte(
    		@RequestParam(value = "id", defaultValue = "1") int id) throws IOException {
		
		AuditoriaEmpresa auditoria = auditoriaEmpresaManager.findById(new Long(id));
		
		ByteArrayInputStream bis = reporteAuditoria.reporteIndividual(auditoria);
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=auditorias.pdf");
        
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
	}
	
	
}
