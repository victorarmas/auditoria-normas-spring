package com.auditoria.proyecto.auditoria.repository;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.auditoria.proyecto.auditoria.model.AuditoriaEmpresa;

@Repository("customAuditoriaEmpresaRepository")
public class AuditoriaEmpresaCustomRepository implements AuditoriaEmpresaRepository{

	@Autowired
	@Qualifier("auditoriaEmpresaRepository")
	private AuditoriaEmpresaRepository auditoriaEmpresaRepository;
	
	@Override
	public <S extends AuditoriaEmpresa> S save(S entity) {
		return auditoriaEmpresaRepository.save(entity);
	}

	@Override
	public <S extends AuditoriaEmpresa> Iterable<S> saveAll(Iterable<S> entities) {
		return auditoriaEmpresaRepository.saveAll(entities);
	}

	@Override
	public Optional<AuditoriaEmpresa> findById(Long id) {
		return auditoriaEmpresaRepository.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		return auditoriaEmpresaRepository.existsById(id);
	}

	@Override
	public Iterable<AuditoriaEmpresa> findAll() {
		return auditoriaEmpresaRepository.findAll();
	}

	@Override
	public Iterable<AuditoriaEmpresa> findAllById(Iterable<Long> ids) {
		return auditoriaEmpresaRepository.findAllById(ids);
	}

	@Override
	public long count() {
		return auditoriaEmpresaRepository.count();
	}

	@Override
	public void deleteById(Long id) {
		auditoriaEmpresaRepository.deleteById(id);
	}

	@Override
	public void delete(AuditoriaEmpresa entity) {
		auditoriaEmpresaRepository.delete(entity);
	}

	@Override
	public void deleteAll(Iterable<? extends AuditoriaEmpresa> entities) {
		auditoriaEmpresaRepository.deleteAll(entities);
	}

	@Override
	public void deleteAll() {
		auditoriaEmpresaRepository.deleteAll();
	}

}
