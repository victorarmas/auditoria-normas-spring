package com.auditoria.proyecto.auditoria.model;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "empresas")
public class Empresa {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idEmpresa;
	
	private String razonSocial;
	private String ruc;
	private String representante;
	
	public Empresa() {
	}

	public Empresa(Long idEmpresa, String razonSocial, String ruc, String representante) {
		super();
		this.idEmpresa = idEmpresa;
		this.razonSocial = razonSocial;
		this.ruc = ruc;
		this.representante = representante;
	}

	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getRepresentante() {
		return representante;
	}

	public void setRepresentante(String representante) {
		this.representante = representante;
	}

	@Override
	public String toString() {
		return "Empresa [idEmpresa=" + idEmpresa + ", razonSocial=" + razonSocial + ", ruc=" + ruc + ", representante="
				+ representante + "]";
	}
	
	
	
}
