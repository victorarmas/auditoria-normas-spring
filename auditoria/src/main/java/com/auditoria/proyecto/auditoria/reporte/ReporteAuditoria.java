package com.auditoria.proyecto.auditoria.reporte;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.auditoria.proyecto.auditoria.model.AuditoriaEmpresa;
import com.auditoria.proyecto.auditoria.model.RespuestaAuditoria;
import com.auditoria.proyecto.auditoria.util.Constante;
import com.auditoria.proyecto.auditoria.util.Util;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

@Component
public class ReporteAuditoria {
	
	@Autowired
	private Util util;

	public ByteArrayInputStream reporteAuditorias(List<AuditoriaEmpresa> auditorias) {
		
		Document documento = new Document();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		NumberFormat formatter = new DecimalFormat("#0.00");
		int titleSize = 18;
		
		try {
			
			Font tituloFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD, titleSize);
			
			Paragraph titulo = new Paragraph("Reporte de Auditorias\n\n", tituloFont);
			
			titulo.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(7);
            table.setWidthPercentage(90);
            table.setWidths(new int[]{5, 9, 15, 10, 14, 9, 15});

            Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);

            PdfPCell hcell;
            hcell = new PdfPCell(new Phrase("Id", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            hcell = new PdfPCell(new Phrase("Norma", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            hcell = new PdfPCell(new Phrase("Empresa", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);
            
            hcell = new PdfPCell(new Phrase("Aplicador", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);
            
            hcell = new PdfPCell(new Phrase("Fecha", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);
            
            hcell = new PdfPCell(new Phrase("Porcentaje", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);
            
            hcell = new PdfPCell(new Phrase("Resultado", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);
            
            for(AuditoriaEmpresa auditoria : auditorias) {
            	
            	PdfPCell cell;
            	
            	cell = new PdfPCell(new Phrase(auditoria.getIdAuditoriaEmpresa().toString()));
            	cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase(auditoria.getNorma().getNombre()));
                //cell.setPaddingLeft(5);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
            	
                cell = new PdfPCell(new Phrase(auditoria.getEmpresa().getRazonSocial()));
                //cell.setPaddingLeft(5);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase(auditoria.getUsuarioAplicador().getUsername()));
                //cell.setPaddingLeft(5);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase(auditoria.getFecha().toString()));
                //cell.setPaddingLeft(5);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                
                double porcentaje = util.getPorcentajeAciertos(auditoria);
                String porcentajeText = formatter.format(porcentaje) + "%";
                
                cell = new PdfPCell(new Phrase(porcentajeText));
                //cell.setPaddingLeft(5);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase(auditoria.getStatus()));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                //cell.setPaddingRight(5);
                table.addCell(cell);
                
            }
            
            PdfWriter.getInstance(documento, out);
            documento.open();
            
            documento.add(titulo);
            documento.add(table);
            
            documento.close();
			
		} catch (DocumentException ex) {
			Logger.getLogger(ReporteAuditoria.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		return new ByteArrayInputStream(out.toByteArray());
	}
	
	public ByteArrayInputStream reporteIndividual(AuditoriaEmpresa auditoria) {
		
		Document documento = new Document();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		NumberFormat formatter = new DecimalFormat("#0.00");
		int titleSize = 18;
		int textLeftSize = 14;
		int preguntaSize = 8;
		int headSize = 12;
		
		try {
			
			Font tituloFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD, titleSize);
			Font subTitleFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD, textLeftSize);
			
			Paragraph titulo = new Paragraph("Reporte \n\n\n", tituloFont);
			titulo.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph subTitleNorma = new Paragraph("Norma: " + auditoria.getNorma().getNombre() + "\n\n", subTitleFont);
			subTitleNorma.setAlignment(Element.ALIGN_LEFT);
			
			Paragraph subTitleEmpresa = new Paragraph("Empresa: " + auditoria.getEmpresa().getRazonSocial() + "\n\n", subTitleFont);
			subTitleEmpresa.setAlignment(Element.ALIGN_LEFT);
			
			Paragraph subTitleFecha = new Paragraph("Fecha: " + auditoria.getFecha() + "\n\n", subTitleFont);
			subTitleFecha.setAlignment(Element.ALIGN_LEFT);
			
			double porcentaje = util.getPorcentajeAciertos(auditoria);
            String porcentajeText = formatter.format(porcentaje);
			
			Paragraph subTitlePorcentaje = new Paragraph("Aciertos: " + porcentajeText + "% \n\n", subTitleFont);
			subTitlePorcentaje.setAlignment(Element.ALIGN_LEFT);
			
			Paragraph subTitleResultado = new Paragraph("Resultado: " + auditoria.getStatus() + "\n\n", subTitleFont);
			subTitleResultado.setAlignment(Element.ALIGN_LEFT);
			
			
			PdfPTable table = new PdfPTable(3);
            table.setWidthPercentage(90);
            table.setWidths(new int[]{25, 55, 15});

            Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD, headSize);
            
            PdfPCell hcell;
            hcell = new PdfPCell(new Phrase("Apartado", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);
            
            
            hcell = new PdfPCell(new Phrase("Pregunta", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);
            
            hcell = new PdfPCell(new Phrase("Resultado", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);
            
            Font preguntaFont = FontFactory.getFont(FontFactory.HELVETICA, preguntaSize);
            
            for(RespuestaAuditoria respuesta : auditoria.getRespuestas()) {
            	PdfPCell cell;
            	
            	cell = new PdfPCell(new Phrase(respuesta.getItemCuestionario().getApartado().getNombre(), preguntaFont));
            	cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase(respuesta.getItemCuestionario().getItem(), preguntaFont));
            	cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                
                String resultado = respuesta.getResultado();
                resultado = Constante.CUMPLE.equals(resultado)? "CUMPLE" : "NO CUMPLE";
                
                cell = new PdfPCell(new Phrase(resultado, preguntaFont));
            	cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
            }
            
            PdfWriter.getInstance(documento, out);
            documento.open();
            
            documento.add(titulo);
            documento.add(subTitleNorma);
            documento.add(subTitleEmpresa);
            documento.add(subTitleFecha);
            documento.add(subTitlePorcentaje);
            documento.add(subTitleResultado);
            documento.add(table);
            
            documento.close();
			
			
		} catch (DocumentException ex) {
			Logger.getLogger(ReporteAuditoria.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		return new ByteArrayInputStream(out.toByteArray());
	}
	
}
