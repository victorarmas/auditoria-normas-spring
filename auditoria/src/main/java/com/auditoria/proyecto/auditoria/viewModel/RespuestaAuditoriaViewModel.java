package com.auditoria.proyecto.auditoria.viewModel;

public class RespuestaAuditoriaViewModel {
	private Long idRespuestaAuditoria;
	private Long idAuditoriaEmpresa;
	private Long idItemCuestionario;
	
	private String respuestaMarcada;
	private String resultado;
	
	public RespuestaAuditoriaViewModel() {
	}

	public RespuestaAuditoriaViewModel(Long idRespuestaAuditoria, Long idAuditoriaEmpresa, Long idItemCuestionario,
			String respuestaMarcada, String resultado) {
		super();
		this.idRespuestaAuditoria = idRespuestaAuditoria;
		this.idAuditoriaEmpresa = idAuditoriaEmpresa;
		this.idItemCuestionario = idItemCuestionario;
		this.respuestaMarcada = respuestaMarcada;
		this.resultado = resultado;
	}

	public Long getIdRespuestaAuditoria() {
		return idRespuestaAuditoria;
	}

	public void setIdRespuestaAuditoria(Long idRespuestaAuditoria) {
		this.idRespuestaAuditoria = idRespuestaAuditoria;
	}

	public Long getIdAuditoriaEmpresa() {
		return idAuditoriaEmpresa;
	}

	public void setIdAuditoriaEmpresa(Long idAuditoriaEmpresa) {
		this.idAuditoriaEmpresa = idAuditoriaEmpresa;
	}

	public Long getIdItemCuestionario() {
		return idItemCuestionario;
	}

	public void setIdItemCuestionario(Long idItemCuestionario) {
		this.idItemCuestionario = idItemCuestionario;
	}

	public String getRespuestaMarcada() {
		return respuestaMarcada;
	}

	public void setRespuestaMarcada(String respuestaMarcada) {
		this.respuestaMarcada = respuestaMarcada;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
	
	
	
}
