package com.auditoria.proyecto.auditoria.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "auditorias_empresa")
public class AuditoriaEmpresa {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idAuditoriaEmpresa;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="id_norma")
	private Norma norma;
	
	//@ManyToOne(cascade=CascadeType.ALL)
	@ManyToOne
	@JoinColumn(name="id_empresa")
	private Empresa empresa;
	
	
	//@ManyToOne
	@ManyToOne(cascade=CascadeType.MERGE)
	@JoinColumn(name="id_usuario_aplicador")
	private Usuario usuarioAplicador;
	
	//private int idUsuarioAplicador;
	
	private Date fecha;
	
	private String status;
	
	@OneToMany(targetEntity=RespuestaAuditoria.class, mappedBy="auditoriaEmpresa", fetch=FetchType.LAZY)
	private List<RespuestaAuditoria> respuestas;
	
	public AuditoriaEmpresa() {
		
	}

	public AuditoriaEmpresa(Long idAuditoriaEmpresa, Norma norma, Empresa empresa, Usuario usuarioAplicador, Date fecha,
			String status, List<RespuestaAuditoria> respuestas) {
		super();
		this.idAuditoriaEmpresa = idAuditoriaEmpresa;
		this.norma = norma;
		this.empresa = empresa;
		this.usuarioAplicador = usuarioAplicador;
		//this.idUsuarioAplicador = 1;
		this.fecha = fecha;
		this.status = status;
		this.respuestas = respuestas;
	}

	public Long getIdAuditoriaEmpresa() {
		return idAuditoriaEmpresa;
	}

	public void setIdAuditoriaEmpresa(Long idAuditoriaEmpresa) {
		this.idAuditoriaEmpresa = idAuditoriaEmpresa;
	}

	public Norma getNorma() {
		return norma;
	}

	public void setNorma(Norma norma) {
		this.norma = norma;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	/*
	public Usuario getUsuarioAplicador() {
		return usuarioAplicador;
	}

	public void setUsuarioAplicador(Usuario usuarioAplicador) {
		this.usuarioAplicador = usuarioAplicador;
	}
	*/
	
	public Date getFecha() {
		return fecha;
	}

	public Usuario getUsuarioAplicador() {
		return usuarioAplicador;
	}

	public void setUsuarioAplicador(Usuario usuarioAplicador) {
		this.usuarioAplicador = usuarioAplicador;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<RespuestaAuditoria> getRespuestas() {
		return respuestas;
	}

	public void setRespuestas(List<RespuestaAuditoria> respuestas) {
		this.respuestas = respuestas;
	}

	

}
