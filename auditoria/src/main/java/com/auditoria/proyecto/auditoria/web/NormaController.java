package com.auditoria.proyecto.auditoria.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.auditoria.proyecto.auditoria.business.ApartadoManager;
import com.auditoria.proyecto.auditoria.business.ItemCuestionarioManager;
import com.auditoria.proyecto.auditoria.business.NormaManager;
import com.auditoria.proyecto.auditoria.model.Apartado;
import com.auditoria.proyecto.auditoria.model.ItemCuestionario;
import com.auditoria.proyecto.auditoria.model.Norma;
import com.auditoria.proyecto.auditoria.repository.ItemCuestionarioRepository;
import com.auditoria.proyecto.auditoria.viewModel.ApartadoViewModel;
import com.auditoria.proyecto.auditoria.viewModel.ItemCuestionarioViewModel;

@Controller
public class NormaController {

	@Autowired
	private NormaManager normaManager;
	
	@Autowired
	private ItemCuestionarioManager itemCuestionarioManager;
	
	@Autowired
	private ApartadoManager apartadoManager;
	
	/**
	 * 
	 * @param model
	 * @return Formulario con lista de normas
	 */
	@GetMapping("/normas")
	public String getNormas(Model model){
		model.addAttribute("normas", normaManager.findAll());
		return "normas";
	}
	
	/**
	 * 
	 * @param model
	 * @return a Formulario para crear una nueva norma
	 */
	@GetMapping("/norma/new")
	public String newNorma(Model model) {
		model.addAttribute("norma",new Norma());
		return "normaForm";
	}
	
	/**
	 * Crea una nueva Norma
	 * @param norma
	 * @param model
	 * @return: Redirecciona a la lista de normas
	 */
	@PostMapping("/norma")
	public String crearNorma(Norma norma, Model model) {
		normaManager.saveNorma(norma);
		return "redirect:/normas";
	}
	
	/**
	 * 
	 * @param id
	 * @param model
	 * @return a Formulario para ver una norma (No vista editar)
	 */
	@GetMapping("/norma/{id}")
	public String getNormaById(@PathVariable Long id, Model model) {
		model.addAttribute("norma", normaManager.findById(id));
		return "norma";
	}
	
	/**
	 * 
	 * @return a formulario para editar una norma por Id
	 */
	@GetMapping("/norma/edit/{id}")
	public String editNorma(@PathVariable Long id, Model model) {
		model.addAttribute("norma", normaManager.findById(id));
		return "normaForm";
	}
	
	/**
	 * Actualiza una norma
	 * @param norma
	 * @return a vista con lista de normas
	 */
	@PostMapping("/norma/{id}")
	public String updateNorma(Norma norma) {
		normaManager.saveNorma(norma);
		return "redirect:/normas";
	}
	
	/**
	 * Elimina una norma por el id indicado
	 * @param id
	 * @return a vista con la lista de normas
	 */
	@GetMapping("/norma/delete/{id}")
	public String deleteNormaById(@PathVariable Long id) {
		normaManager.deleteById(id);
		return "redirect:/normas";
	}
	
	/**
	 * 
	 * @param id
	 * @param model
	 * @return a vista de apartados de una norma
	 */
	@GetMapping("/norma/apartado/{id}")
	public String getApartadoById(@PathVariable Long id, Model model) {
		ApartadoViewModel apartadoViewModel = new ApartadoViewModel();
		apartadoViewModel.setIdNorma(id);
		
		Norma norma = normaManager.findById(id);
		model.addAttribute("norma", norma);
		model.addAttribute("apartado", apartadoViewModel);
		
		return "apartado";
	}
	
	/**
	 * Crea un nuevo apartado de la norma
	 * @param apartadoForm
	 * @param model
	 * @return
	 */
	@PostMapping("/norma/apartado")
	public String crearApartado(ApartadoViewModel apartadoForm, Model model) {
		Norma norma = normaManager.findById(apartadoForm.getIdNorma());
		
		Apartado apartado = new Apartado();
		apartado.setNombre(apartadoForm.getNombre());
		apartado.setNorma(norma);
		
		apartadoManager.save(apartado);
		
		return "redirect:/norma/apartado/" + norma.getIdNorma();
	}
	
	/**
	 * Elimina un apartado
	 * @param id
	 * @param normaId
	 * @return
	 */
	@GetMapping("/norma/apartado/delete/{normaId}/{id}")
	public String deleteApartado(@PathVariable Long id, @PathVariable Long normaId) {
		apartadoManager.deleteById(id);
		return "redirect:/norma/apartado/" + normaId;
	}
	
	/**
	 * 
	 * @param id
	 * @param model
	 * @return a vista del cuestionario de una norma
	 */
	@GetMapping("/norma/cuestionario/{id}")
	public String getCuestionarioById(@PathVariable Long id, Model model) {
		ItemCuestionarioViewModel cuestionario = new ItemCuestionarioViewModel();
		cuestionario.setIdNorma(id);
		
		Norma norma = normaManager.findById(id);
		model.addAttribute("norma", norma);
		//model.addAttribute("apartados", apartadoManager.findAll());
		model.addAttribute("cuestionario", cuestionario);
		return "cuestionario";
	}
	
	/**
	 * Registrar un nuevo item del cuestionario
	 * @param itemCuestionario
	 * @param norma
	 * @param model
	 * @return 
	 */
	@PostMapping("/norma/cuestionario")
	public String crearItemCuestionario(ItemCuestionarioViewModel itemCuestionarioForm, Model model) {
		//Norma norma = normaManager.findById(itemCuestionarioForm.getIdNorma());
		Apartado apartado = apartadoManager.findById(itemCuestionarioForm.getIdApartado());
		
		ItemCuestionario itemCuestionario = new ItemCuestionario();
		itemCuestionario.setItem(itemCuestionarioForm.getItem());
		itemCuestionario.setRespuestaDeseada(itemCuestionarioForm.getRespuesta());
		itemCuestionario.setApartado(apartado);
		
		itemCuestionarioManager.saveItem(itemCuestionario);
		return "redirect:/norma/cuestionario/" + itemCuestionarioForm.getIdNorma(); 
	}
	
	/**
	 * Eliminar un item del cuestionario
	 * @param id
	 * @param normaId
	 * @return a la vista de edicion de los items
	 */
	@GetMapping("/norma/cuestionario/delete/{normaId}/{id}")
	public String deleteItem(@PathVariable Long id, @PathVariable Long normaId) {
		itemCuestionarioManager.deleteById(id);
		return "redirect:/norma/cuestionario/" + normaId;
	}
}
