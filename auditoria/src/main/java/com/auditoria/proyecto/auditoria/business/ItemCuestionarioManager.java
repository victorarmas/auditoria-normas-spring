package com.auditoria.proyecto.auditoria.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.auditoria.proyecto.auditoria.model.ItemCuestionario;
import com.auditoria.proyecto.auditoria.repository.ItemCuestionarioRepository;

@Service
public class ItemCuestionarioManager {

	@Autowired
	private ItemCuestionarioRepository itemCuestionarioRepository;
	
	public void saveItem(ItemCuestionario item) {
		itemCuestionarioRepository.save(item);
	}
	
	public void deleteById(Long id) {
		itemCuestionarioRepository.deleteById(id);
	}
	
}
