package com.auditoria.proyecto.auditoria.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.auditoria.proyecto.auditoria.util.Constante;

@Entity
@Table(name = "respuestas_auditoria")
public class RespuestaAuditoria {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idRespuestaAuditoria;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="id_auditoria_empresa")
	private AuditoriaEmpresa auditoriaEmpresa;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="id_item_cuestionario")
	private ItemCuestionario itemCuestionario;
	
	private String respuestaMarcada;
	private String resultado;
	
	public RespuestaAuditoria() {
	}

	public RespuestaAuditoria(Long idRespuestaAuditoria, AuditoriaEmpresa auditoriaEmpresa,
			ItemCuestionario itemCuestionario, String respuestaMarcada, String resultado) {
		super();
		this.idRespuestaAuditoria = idRespuestaAuditoria;
		this.auditoriaEmpresa = auditoriaEmpresa;
		this.itemCuestionario = itemCuestionario;
		this.respuestaMarcada = respuestaMarcada;
		this.resultado = resultado;
	}

	public Long getIdRespuestaAuditoria() {
		return idRespuestaAuditoria;
	}

	public void setIdRespuestaAuditoria(Long idRespuestaAuditoria) {
		this.idRespuestaAuditoria = idRespuestaAuditoria;
	}

	public AuditoriaEmpresa getAuditoriaEmpresa() {
		return auditoriaEmpresa;
	}

	public void setAuditoriaEmpresa(AuditoriaEmpresa auditoriaEmpresa) {
		this.auditoriaEmpresa = auditoriaEmpresa;
	}

	public ItemCuestionario getItemCuestionario() {
		return itemCuestionario;
	}

	public void setItemCuestionario(ItemCuestionario itemCuestionario) {
		this.itemCuestionario = itemCuestionario;
	}

	public String getRespuestaMarcada() {
		return respuestaMarcada;
	}

	public void setRespuestaMarcada(String respuestaMarcada) {
		this.respuestaMarcada = respuestaMarcada;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
	
	public boolean checked() {
		return Constante.SI.equals(this.respuestaMarcada);
	}
	
	
}
