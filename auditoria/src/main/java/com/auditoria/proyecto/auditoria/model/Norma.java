package com.auditoria.proyecto.auditoria.model;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "normas")
public class Norma {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idNorma;
	
	private String nombre;
	
	@OneToMany(targetEntity=Apartado.class, mappedBy="norma", fetch=FetchType.EAGER)
	private List<Apartado> apartados = new ArrayList<Apartado>() ;
	
	public Norma() {
	}

	public Norma(Long idNorma, String nombre) {
		this.idNorma = idNorma;
		this.nombre = nombre;
	}

	public Long getIdNorma() {
		return idNorma;
	}

	public void setIdNorma(Long idNorma) {
		this.idNorma = idNorma;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	

	public List<Apartado> getApartados() {
		return apartados;
	}

	public void setApartados(List<Apartado> apartados) {
		this.apartados = apartados;
	}

	@Override
	public String toString() {
		return "Norma [idNorma=" + idNorma + ", nombre=" + nombre + ", apartados=" + apartados + "]";
	}
	
	
}
