package com.auditoria.proyecto.auditoria.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.auditoria.proyecto.auditoria.model.Usuario;
import com.auditoria.proyecto.auditoria.repository.UsuarioRepository;
import com.auditoria.proyecto.auditoria.util.Util;

@Service
public class UsuarioManager {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private Util util;
	
	public Usuario login(Usuario usuario) {
		String password = util.getMD5EncryptedValue(usuario.getPassword());
		return usuarioRepository.findByUsernameAndPassword(usuario.getUsername(), password);
	}
	
	public void save(Usuario usuario) {
		usuario.setPassword(util.getMD5EncryptedValue(usuario.getPassword()));
		usuarioRepository.save(usuario);
	}
	
	public Usuario findById(Long id) {
		return usuarioRepository.findById(id).orElse(new Usuario());
	}
	
	public List<Usuario> findAll(){
		Iterable<Usuario> usuariosIterable = usuarioRepository.findAll();
		List<Usuario> usuarios = new ArrayList<Usuario>();
		usuariosIterable.forEach(usuarios::add);
		System.out.println("===================");
		System.out.println(usuarios);
		System.out.println("===================");
		return usuarios;
	}
	
	public void deleteById(Long id) {
		usuarioRepository.deleteById(id);
	}
	
}
