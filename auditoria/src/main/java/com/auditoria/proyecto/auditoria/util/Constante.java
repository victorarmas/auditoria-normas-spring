package com.auditoria.proyecto.auditoria.util;

public class Constante {

	public static String SI = "SI";
	public static String NO = "NO";
	
	public static String CUMPLE = "CORRECTO";
	public static String NO_CUMPLE = "INCORRECTO";
	
	public static String APROBADO = "APROBADO";
	public static String NO_APROBADO = "DESAPROBADO";
	
}
