package com.auditoria.proyecto.auditoria.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.auditoria.proyecto.auditoria.model.Norma;
import com.auditoria.proyecto.auditoria.repository.NormaRepository;

@Service
public class NormaManager {
	
	@Autowired
	private NormaRepository normaRepository;
	
	public void saveNorma(Norma norma) {
		normaRepository.save(norma);
	}
	
	public Norma findById(Long id) {
		return normaRepository.findById(id).orElse(new Norma());
	}
	
	public List<Norma> findAll(){
		Iterable<Norma> normasIterator =normaRepository.findAll(); 
		List<Norma> normas = new ArrayList<Norma>();
		normasIterator.forEach(normas::add);
		System.out.println("===================");
		System.out.println(normas);
		System.out.println("===================");
		return normas;
	}
	
	public void deleteById(Long id) {
		normaRepository.deleteById(id);
	}
	
}
