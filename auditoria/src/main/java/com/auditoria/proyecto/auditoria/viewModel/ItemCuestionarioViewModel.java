package com.auditoria.proyecto.auditoria.viewModel;

public class ItemCuestionarioViewModel {
	private Long idNorma;
	private Long idApartado;
	private String item;
	private String respuesta;
	
	
	public ItemCuestionarioViewModel() {
	}


	public ItemCuestionarioViewModel(Long idNorma, Long idApartado, String item, String respuesta) {
		super();
		this.idNorma = idNorma;
		this.idApartado = idApartado;
		this.item = item;
		this.respuesta = respuesta;
	}


	public Long getIdNorma() {
		return idNorma;
	}


	public void setIdNorma(Long idNorma) {
		this.idNorma = idNorma;
	}


	public Long getIdApartado() {
		return idApartado;
	}


	public void setIdApartado(Long idApartado) {
		this.idApartado = idApartado;
	}


	public String getItem() {
		return item;
	}


	public void setItem(String item) {
		this.item = item;
	}


	public String getRespuesta() {
		return respuesta;
	}


	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	
}
