package com.auditoria.proyecto.auditoria.web;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.auditoria.proyecto.auditoria.business.UsuarioManager;
import com.auditoria.proyecto.auditoria.model.Usuario;

@Controller
public class LoginController {

	@Autowired
	private UsuarioManager usuarioManager;
	
	
	/**
	 * 
	 * @param model
	 * @return a formulario de login
	 */
	@GetMapping("/login")
	public String loginForm(Model model) {
		model.addAttribute("usuario", new Usuario());
		return "login";
	}
	
	@PostMapping("/login")
	public String login(Usuario usuario, HttpSession session) {
		Usuario usuarioExiste = usuarioManager.login(usuario);
		
		System.out.println("==============USUAIO EXISTE===========");
		System.out.println(usuarioExiste);
		System.out.println("======================================");
		
		if(usuarioExiste != null) {
			//session.setAttribute("usuarioLogueado", usuario);
			session.setAttribute("usuarioLogueado", usuarioExiste);
			return "redirect:/home";
		}
		return "redirect:/login";
	}
	
}
